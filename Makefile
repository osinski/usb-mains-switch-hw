# Marcin Osiński 2022

KICAD_INTERACTIVE_BOM_SCRIPT = $(shell find / \
							   -name 'generate_interactive_bom.py'\
							   2>&1 | grep -v 'Permission denied')

PROJECT_NAME = usb-mains-switch
SCH_FILES = $(shell find ./ -name '*.kicad_sch')
#PCB_FILE = $(PROJECT_NAME).kicad_pcb
PCB_FILE = $(shell find ./ -name '*.kicad_pcb')

MFG_FILES_DIR = ./outputs
PLOT_FILES_DIR = ./plots-imgs



all: erc drc fabrication-outputs assembly-outputs bom schematics 3d-model pcb-drawings layer-plots


#TBD
fabrication-outputs:
	@echo "--- Generating Board Fabrication Outputs ---"

	@echo "--- Zipping them up! ---"
	@zip -r $(MFG_FILES_DIR)/gerbers.zip \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Cu.gtl \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Cu.gbl \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Mask.gts \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Mask.gbs \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Paste.gtp \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Paste.gbp \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-F_Silkscreen.gto \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-B_Silkscreen.gbo \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-Edge_Cuts.gm1 \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-NPTH-drl_map.gbr \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-NPTH.drl \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-PTH-drl_map.gbr \
		$(MFG_FILES_DIR)/$(PROJECT_NAME)-PTH.drl 
	@rm $(MFG_FILES_DIR)/*.g* $(MFG_FILES_DIR)/*.drl

	@echo "--- Done! ---"


assembly-outputs: manual-assembly


manual-assembly: $(PCB_FILE)
	@echo "--- Generating Interactive BOM file in\
		$(MFG_FILES_DIR)/Interactive-BOM ---"
	@eeschema_do netlist $(PROJECT_NAME).kicad_sch ./
	@python $(KICAD_INTERACTIVE_BOM_SCRIPT) \
		--dark-mode\
		--layer-view F\
		--netlist-file $(PROJECT_NAME).net\
		--extra-fields "Mfr,MPN,Description"\
		--dest-dir $(MFG_FILES_DIR)/Interactive-BOM\
		--no-browser\
		$(PCB_FILE)
	@rm $(PROJECT_NAME).net
	@echo "--- Done! ---"


automated-assembly: $(PCB_FILE)
	@echo "--- Generating Pick n Place Files in $(MFG_FILES_DIR) ---"

	@echo "--- Done! ---"


schematics: $(SCH_FILES)
	@echo "--- Generating PDF Schematics in $(PLOT_FILES_DIR) ---"
	@eeschema_do export --monochrome --all_pages \
						--output_name schematic-prints.pdf \
						$(PROJECT_NAME).kicad_sch $(PLOT_FILES_DIR)

	@echo "--- Done! ---"


bom: $(SCH_FILES)
	@echo "--- Generating Bill of Materials in $(MFG_FILES_DIR) ---"
	@eeschema_do bom_xml $(PROJECT_NAME).kicad_sch ./
	@python -m kibom --number 1 --variant Default --cfg ./kibom_settings.ini\
					 --subdirectory $(MFG_FILES_DIR)\
					 $(PROJECT_NAME).xml\
					 $(MFG_FILES_DIR)/$(PROJECT_NAME).csv
	@rm $(PROJECT_NAME).xml
	@echo "--- Done! ---"


layer-plots: $(PCB_FILE)
	@echo "--- Generating PCB Layer Plots in $(PLOT_FILES_DIR) ---"
	@pcbnew_do export --monochrome \
		$(PCB_FILE) $(PLOT_FILES_DIR) Edge.Cuts User.1 F.Cu
	@mv $(PLOT_FILES_DIR)/printed.pdf $(PLOT_FILES_DIR)/pcb-layers-top.pdf

	@pcbnew_do export --monochrome \
		$(PCB_FILE) $(PLOT_FILES_DIR) Edge.Cuts User.1 B.Cu
	@mv $(PLOT_FILES_DIR)/printed.pdf $(PLOT_FILES_DIR)/pcb-layers-bottom.pdf

	@pcbnew_do export --monochrome \
		$(PCB_FILE) $(PLOT_FILES_DIR) Edge.Cuts User.1 F.Silkscreen
	@mv $(PLOT_FILES_DIR)/printed.pdf $(PLOT_FILES_DIR)/pcb-layers-silk.pdf

	@pcbnew_do export --monochrome \
		$(PCB_FILE) $(PLOT_FILES_DIR) Edge.Cuts User.1 F.Fab
	@mv $(PLOT_FILES_DIR)/printed.pdf $(PLOT_FILES_DIR)/pcb-layers-assembly.pdf

	@gs -dQUIET \
		-dBATCH \
		-dNOPAUSE \
		-sDEVICE=pdfwrite \
		-sOutputFile=$(PLOT_FILES_DIR)/pcb-layers-merged.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-top.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-bottom.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-silk.pdf \
		$(PLOT_FILES_DIR)/pcb-layers-assembly.pdf
	@echo "--- Done! ---"


pcb-drawings: $(PCB_FILE)
	@echo "--- Generating PCB Drawings in $(PLOT_FILES_DIR) ---"
	@pcbdraw plot --side front --silent \
		$(PCB_FILE) $(PLOT_FILES_DIR)/board-top.png
	@pcbdraw plot --side back --silent \
		$(PCB_FILE) $(PLOT_FILES_DIR)/board-back.png

	@echo "--- Done! ---"


#TBD
3d-model: $(PCB_FILE)
	@echo "--- (NOT YET) Generating 3D Model of PCB in $(MFG_FILES_DIR) ---"

	@echo "--- Done! ---"


erc: $(SCH_FILES)
	@echo "--- Running Electrical Rules Check ---"
	@eeschema_do run_erc $(PROJECT_NAME).kicad_sch $(MFG_FILES_DIR)
	@echo "--- Done! ---"


drc: $(PCB_FILE)
	@echo "--- Running Design Rules Check ---"
	@pcbnew_do run_drc $(PCB_FILE) $(MFG_FILES_DIR)
	@echo "--- Done! ---"


clean:
	@echo "--- Removing Generated Files ---"
	@rm -rf $(MFG_FILES_DIR)/Interactive-BOM
	@rm $(MFG_FILES_DIR)/*
	@rm $(PLOT_FILES_DIR)/*.pdf $(PLOT_FILES_DIR)/board-*.png

	@echo "--- Done! ---"

