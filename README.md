# *USB Mains Switch* Kicad Project Repository

## Preview screenshots

<p align="center">
	<img src="./plots-imgs/board-top.png"  width="40%"  />
	<img src="./plots-imgs/board-back.png" width="40%"  />
	<img src="./plots-imgs/preview-box-open.png" />
	<img src="./plots-imgs/preview-box-front.png" width="45%"/>
	<img src="./plots-imgs/preview-box-rear.png" width="45%"/>
</p>

## About

Purpose of this device is to pass-through mains voltage when `5V` presence on
**Main USB Connector** is detected. Example use case could be to automatically
supply mains voltage to audio amplifier when a TV is switched on. An additional
switch on front panel is provided to by-pass the relay and enable the
mains-connected device anyway.

USB used to detect `5V` (TV is enabled) is also provided on front panel.
*USB HS Signal Conditioner* is provided to compensate for signal losses on
increased length of connection.

Additional switched USB connectors are provided on front panel. They multiplex
**Switched USB Connector** (type-B µ connector on rear panel). One of the front
panel *USB Type-A* connectors is switched to rear panel *Type-B µ* connector
when `5V` on **Main USB Connector** is detected and the other in opposite case.

Four **LEDs** on front panel indicate which connector is *active*, the one
above front panel rocker switch indicating that mains voltage is passed through
the device.

Front and rear panels for the enclosure should be 3D-printed. *STEP* models
and *FreeCAD* design files for them are in `enclosure` directory. *STEP* models
of PCB, enclosure and additional components are also there, as well as
*FreeCAD* project with whole device visualisation, from which
`preview-box-*.png` images embedded within this readme come from.


## Output files

*Gerber* and *Drill* (prepared according to **JLCPCB** specification) files are
zipped together and reside in `outputs` directory. *CSV* *BOM* file,
*interactive BOM* *html* file and *ERC* and *DRC* reports are also in there.

Schematic and PCB layer prints, as well as some preview images reside in
`plots-imgs` directory.


